return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  config = function()
    local configs = require("nvim-treesitter.configs")

    configs.setup({
      ensure_installed = { "c", "lua", "python", "bash", "rust", "nim", "java" },
      sync_install = false,
      highlight = {
        enable = true,
      },
      indent = {
        enable = false,
      },
    })
  end,
}
