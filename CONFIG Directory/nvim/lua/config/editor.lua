vim.opt.mouse = "a"
vim.opt.number = true
vim.opt.relativenumber = true
-- Tabulation configuration
vim.opt.expandtab = true  -- Convert tabs to spaces
vim.opt.shiftwidth = 4    -- Indentation levels = 4 spaces
vim.opt.tabstop = 4       -- A tab character is equivalent to 4 spaces
vim.opt.smarttab = true   -- Smart handling of tabs when inserting
