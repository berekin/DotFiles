vim.keymap.set("i", "<S-Tab>", "<C-v><Tab>", { noremap = true, silent = true })
vim.keymap.set('n', '<leader>pv', ':NvimTreeToggle<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '<leader>pc', ':rightbelow vsplit | terminal<CR>', { noremap = true, silent = true })
vim.keymap.set("n", "<leader>px", ":Ex<CR>", { noremap = true, silent = true })
