# Contains custom aliases
# some can be found in the ~/bashrc or ~/vimrc
# but this file is here to centralise them
# For both bash and zsh

#Markdown viewer
#alias mdview="~/.mdview/main.sh"

#Put the warning of GCC
alias gcc="gcc -Wall -Wextra"

#Mount and unmount the music repertory of the nas with my user
#alias nasmusic="sudo mount.cifs //192.168.1.60/Stockage-Musique ~/NAS -o user=terramotu"
#alias unasmusic="sudo umount ~/NAS"

#launch nvim in place of vi vim
alias vi="nvim"
alias vim="nvim"
alias :q="exit"
#Calm the easter egg.
alias sl="sl -e"

#Go to library quickly
alias bib="cd ~/Documents/bibliotheque"

#Push the library quickly
alias bibpush="cd ~/Documents/bibliotheque &&\
git add . &&\
git commit -m 'sync' &&\
git push;\
cd - &>/dev/null"

#Pull the library quickly
alias bibpull="cd ~/Documents/bibliotheque && \
git pull;\
cd - &>/dev/null"

#Launch Exegol
#alias exegol='sudo -E $(which exegol)'

#Fonction qui permet de se déplacer dans un répo
rep() {
	local rep_path=/home/$USER/Repository
		
	#echo "n:$# arg1:$1 arg2:$2"
	if [ $# -eq 0 ] ;
	then
		cd $rep_path
	else
		cd $rep_path/$1
	fi
}

#Ajout de certaines fonctionnalités
#. "$HOME/.cargo/env"

#Append stuff to the end of the $PATH
#For lvim
#export PATH=$PATH:/home/terramotu/.local/bin
#For nim
#export PATH=$PATH:/home/terramotu/.nimble/bin
