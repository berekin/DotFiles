#!/bin/sh
#encoding:utf-8

if [ ! $# -eq 1 ] ; then
	echo "mdview takes only 1 argument: filepath ($# given)"
	return 1
fi

if [ ! -f $1 ] ; then
	echo "File $1 does not exist"
	return 1
fi

# Default values
style="gfm"
mdformat="markdown_strict"

[ -f ~/.mdview/options ] && . ~/.mdview/options
echo "Opening $1"
echo "Using $style css style with $mdformat markdown format"

crtfolder=$HOME/.mdview

filename=$(basename $1)
markdown=$(pandoc -f $mdformat -t html $1)
mdcss=$(cat $crtfolder/styles/$style.css)
tempcss=$(cat $crtfolder/template.css)

viewfile=/tmp/md-view-file
. $crtfolder/template.html > $viewfile

# Display using firefox
( firefox $viewfile &>/dev/null & )

